﻿using EveryDayEats.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace EveryDayEats.Data.Repository
{
    public class UserRepository
    {
        public List<User> GetUseList()
        {

            List<User> userList = new List<User>();
            using (SqlConnection con = new SqlConnection(DataConstant.SQL_CONNECTION))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from tblUser", con);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    userList.Add(new User
                    {
                        Id = (int)dr["Id"],
                        FirstName = dr["FirstName"].ToString(),
                        MiddleName = dr["MiddleName"].ToString(),
                        LastName = dr["LastName"].ToString(),
                        MobileNo = dr["MobileNo"].ToString(),
                        EmailId = dr["EmailId"].ToString()
                    });
                }
            }
           return userList;
        }

        public bool InsertUser(User user)
        {
            using (SqlConnection con = new SqlConnection(DataConstant.SQL_CONNECTION))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO tblUser(FirstName,MiddleName,LastName,MobileNo,EmailId) VALUES(@FirstName,@MiddleName,@LastName,@MobileNo,@EmailId)", con);

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = user.FirstName;
                cmd.Parameters.Add("@MiddleName", SqlDbType.VarChar).Value = user.MiddleName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = user.LastName;
                cmd.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = user.MobileNo;
                cmd.Parameters.Add("@EmailId", SqlDbType.VarChar).Value = user.EmailId;

                int i = cmd.ExecuteNonQuery();
                if (i==1)
                {
                    return true;
                }

            }
            return false;
        }
    }
}
