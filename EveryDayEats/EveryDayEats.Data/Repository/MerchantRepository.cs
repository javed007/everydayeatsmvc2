﻿using EveryDayEats.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EveryDayEats.Data.Repository
{
    public class MerchantRepository
    {
        public List<Merchant> GetMerchantList()
        {
            List<Merchant> merchantList = new List<Merchant>();
            using (SqlConnection con = new SqlConnection(DataConstant.SQL_CONNECTION))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM tblMerchant", con);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    merchantList.Add(new Merchant
                    {
                        Id = (int)dr["Id"],
                        MerchantTypeId = (int)dr["MerchantTypeId"],
                        MerchantName = dr["MerchantName"].ToString(),
                        MerchantDesc = dr["MerchantDesc"].ToString(),
                        Tax = Convert.ToDecimal(dr["Tax"]),
                        MinOrderAmount = Convert.ToDecimal(dr["MinOrderAmount"]),
                        Img = dr["Img"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"])
                    });
                }
            }
            return merchantList;
        }

        public bool DeleteMerchant(int merchantId)
        {
           
            using (SqlConnection con = new SqlConnection(DataConstant.SQL_CONNECTION))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("delete From tblMerchant where Id="+merchantId+"",con);
                int i = cmd.ExecuteNonQuery();
                if (i==1)
                {
                    return true;
                }
                
            }
            return false;
        }
    }
}
