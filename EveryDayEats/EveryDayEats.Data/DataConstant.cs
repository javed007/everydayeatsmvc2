﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EveryDayEats.Data
{
    public class DataConstant
    {
        public static readonly string SQL_CONNECTION= ConfigurationManager.ConnectionStrings["SQL_CONNECTION"].ToString();
    }
}
