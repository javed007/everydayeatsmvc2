﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using EveryDayEats.Data.Repository;
using EveryDayEats.Domain.Entities;

namespace EveryDayEats.Services
{

    public class MerchantService
    {
        MerchantRepository _merchantRepository = new MerchantRepository();
        public List<Merchant> GetMerchantList()
        {
            return _merchantRepository.GetMerchantList();
        }
        public bool DeleteMerchant(int merchantId)
        {
            return _merchantRepository.DeleteMerchant(merchantId);
        }
    }
}
