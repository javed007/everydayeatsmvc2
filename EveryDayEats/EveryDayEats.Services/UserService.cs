﻿using EveryDayEats.Data.Repository;
using EveryDayEats.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EveryDayEats.Services
{
    public class UserService
    {
        UserRepository _userRepository = new UserRepository();
        public List<User> GetUseList()
        {
            return _userRepository.GetUseList();
        }
        public bool InsertUser(User user)
        {
           return _userRepository.InsertUser(user);
        }
    }

}
