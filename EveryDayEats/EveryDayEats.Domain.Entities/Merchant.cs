﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EveryDayEats.Domain.Entities
{
    public class Merchant
    {
        public int Id { get; set; }
        public int MerchantTypeId { get; set; }
        public string MerchantName { get; set; }
        public string MerchantDesc { get; set; }
        public decimal Tax { get; set; }
        public decimal MinOrderAmount { get; set; }
        public string Img { get; set; }
        public bool IsActive { get; set; }
    }
}
