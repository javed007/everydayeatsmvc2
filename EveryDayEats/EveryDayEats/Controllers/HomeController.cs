﻿using EveryDayEats.Domain.Entities;
using EveryDayEats.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EveryDayEats.Controllers
{
    public class HomeController : Controller
    {
        UserService userService = new UserService();
        public ActionResult Index()
        {
            List<User> userList= userService.GetUseList();
            return View(userList);
        }
        [HttpPost]
        public ActionResult InsertUser(User user)
        {
           bool result= userService.InsertUser(user);
            //if (result==true)
            //{

            //}
            return View();
        }




        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}