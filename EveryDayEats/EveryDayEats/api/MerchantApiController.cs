﻿using EveryDayEats.Domain.Entities;
using EveryDayEats.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EveryDayEats.api
{
    public class MerchantApiController : ApiController
    {
        MerchantService merchantService = new MerchantService();

        [HttpGet]
        [Route("api/GetMerchantList")]
        public IHttpActionResult GetMerchantList()
        {
            List<Merchant> merchantList = merchantService.GetMerchantList();
           return Ok(merchantList);
        }
    }
}
